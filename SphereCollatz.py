#!/usr/bin/env python3

import sys

'''---------------READ INPUT---------------'''
# collatz_read
# takes in a string (s)
# returns a list of two ints, representing the beginning and end of a range, [i, j]
def collatz_read(s):
    a = s.split()
    return [int(a[0]), int(a[1])]

'''---------------SOLVE COLLATZ CONJECTURE---------------'''

# populate a cache using powers of 2
cache = {}
for i in range(21):
    cache[2**i] = i + 1

# helper function for collatz_eval
# takes in a number and computes the cycle length
def collatz_helper(num):

    # perform collatz algorithm until num is recognized in cache
    cycle = 0
    while (num not in cache):
        if num % 2 == 0:
            num = num // 2
        else :
            num = num * 3 + 1
        cycle += 1

    # return computed cycle length plus what is already stored in cache
    return cycle + cache[num]


# i is the beginning of the range, inclusive
# j is the end of the range, inclusive
# return the max cycle length of the range [i, j]
def collatz_eval (i, j):
    # ensure valid input
    assert 0 < i,j < 1000000
    i = min(i,j)
    j = max(i,j)
    
    # compute collatz cycle for all [i,j]
    # update cycle variable any time a new max is found
    cycle = 0
    for n in range (i, j+1):
    
        # if n hasn't been cached yet, compute cycle length and update cache
        if n not in cache:
            cache[n] = collatz_helper(n)

        # update cycle variable if new max is found
        cycle = max(cache[n], cycle)
    
    return cycle
    

'''---------------WRITE OUTPUT---------------'''
# collatz_print
# takes in a writer (w), interval [i,j], and max cycle length (v)
# prints three ints
def collatz_print(w, i, j, v):
    w.write(str(i) + ' ' + str(j) + ' ' + str(v) + '\n')


def get_num_lines(r):
    num_lines = 0
    for line in r.readlines():
        num_lines += 1
    return num_lines


def collatz_solve(r, w):
    num_lines = get_num_lines(r)
    counter = 1
    for s in r:
        i,j = collatz_read(s)
        v = collatz_eval(i, j)
        if counter < num_lines:
            collatz_print(w, i, j, v)
        else:
            w.write(str(i) + ' ' + str(j) + ' ' + str(v))
        counter += 1



'''---------------RUN COLLATZ---------------'''
if __name__ == "__main__":
    collatz_solve(sys.stdin, sys.stdout)

