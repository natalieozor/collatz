#!/usr/bin/env python3

# ---------------------------
# projects/collatz/Collatz.py
# Copyright (C) 2016
# Glenn P. Downing
# ---------------------------

# ------------
# collatz_read
# ------------


def collatz_read(s):
    """
    read two ints
    s a string
    return a list of two ints, representing the beginning and end of a range, [i, j]
    """
    a = s.split()
    return [int(a[0]), int(a[1])]

# ------------
# collatz_eval
# ------------

# populate a cache using powers of 2
cache = {}
for i in range(21):
    cache[2**i] = i + 1

# helper function for collatz_eval - takes in a number and computes the cycle length
def collatz_helper(num):

    # perform collatz algorithm until num is recognized in cache
    cycle = 0
    while num not in cache:
        if num % 2 == 0:
            num = num // 2
        else :
            num = num * 3 + 1
        cycle += 1

    # Return the current cycle length plus if there is a cache value
    return cycle + cache[num]


def collatz_eval(i, j):
    """
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    return the max cycle length of the range [i, j]
    """
    
    # ensure valid input
    assert 0 < i,j < 1000000
    i = min(i,j)
    j = max(i,j)
    
    # compute collatz cycle for all [i,j]
    # update cycle variable any time a new max is found
    cycle = 0
    for n in range(i, j+1):
    
        # if n hasn't been cached yet, compute cycle length and update cache
        if n not in cache:
            cache[n] = collatz_helper(n)

        # update cycle variable if new max is found
        cycle = max(cache[n], cycle)
    
    return cycle


# -------------
# collatz_print
# -------------


def collatz_print(w, i, j, v):
    """
    print three ints
    w a writer
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    v the max cycle length
    """
    w.write(str(i) + " " + str(j) + " " + str(v) + "\n")

# -------------
# collatz_solve
# -------------


def collatz_solve(r, w):
    """
    r a reader
    w a writer
    """
    for s in r:
        i, j = collatz_read(s)
        v = collatz_eval(i, j)
        collatz_print(w, i, j, v)